@extends('layout.master')

@section('title', 'Read Database')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('content')
    <table class="table table-dark table-striped table-hover">
        <thead>
            <tr>
                <th>id</th>
                <th>fname</th>
                <th>lname</th>
                <th>age</th>
                <th>created_at</th>
                <th>updated_at</th>
                <th>action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($people as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->fname }}</td>
                    <td>{{ $p->lname }}</td>
                    <td>{{ $p->age }}</td>
                    <td>{{ $p->created_at }}</td>
                    <td>{{ $p->updated_at }}</td>
                    <td>
                        <div class="btn-group">
                            <form action="people/{{ $p->id }}/edit">
                                <button class="btn btn-success" type="submit">Edit</button>
                            </form>
                            <form action="{{ url('people', [$p->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-delete" type="submit">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <form action="/people/create">
        <button class="btn btn-primary btn-block btn-lg" type="submit">Create</button>
    </form>
@endsection

@section('js')
    <script>
        
    </script>
@show