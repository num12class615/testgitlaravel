@extends('layout.master')

@section('title', 'Page Title')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
    <table class="table table-dark">
        <thead>
            <tr>
                <th>id</th>
                <th>fname</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($people as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->fname }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('js')
    <script>
        
    </script>
@show