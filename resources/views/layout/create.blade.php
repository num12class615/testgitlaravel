@extends('layout.master')

@section('title', 'Create Database')
@section('title-class', 'text-primary')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="card bg-dark">
        <div class="card-body">
            <h1 class="text-primary">Add New People</h1>
            <hr>
            <form action="/people" method="POST">
                @csrf
                <div class="formgroup">
                    <label class="text-primary">Firstname</label>
                    <input class="form-control" type="text" name="fname">
                </div>
                <div class="form-group">
                    <label class="text-primary">Lastname</label>
                    <input class="form-control" type="text" name="lname">
                </div>
                <div class="form-group">
                    <label class="text-primary">Age</label>
                    <input class="form-control" type="text" name="age">
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger" >
                        <ul>
                            @foreach ($errors->all() as $errors)
                                <li>{{ $errors }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
            </form>
        </div>
    </div>
    
@endsection

@section('js')
    <script>
        
    </script>
@show