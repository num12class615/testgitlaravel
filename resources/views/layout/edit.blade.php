@extends('layout.master')

@section('title', 'Edit Database')
@section('title-class', 'text-success')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('content')
    <div class="card bg-dark">
        <div class="card-body">
            <h1 class="text-success">Edit People</h1>
            <hr>
            <form action="{{ url('people', [$people->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="formgroup">
                    <label class="text-success">Firstname</label>
                    <input class="form-control" type="text" name="fname" value="{{ $people->fname }}">
                </div>
                <div class="form-group">
                    <label class="text-success">Lastname</label>
                    <input class="form-control" type="text" name="lname" value="{{ $people->lname }}">
                </div>
                <div class="form-group">
                    <label class="text-success">Age</label>
                    <input class="form-control" type="text" name="age" value="{{ $people->age }}">
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger" >
                        <ul>
                            @foreach ($errors->all() as $errors)
                                <li>{{ $errors }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <button class="btn btn-success btn-lg btn-block" type="submit">Edit</button>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script>
        
    </script>
@show