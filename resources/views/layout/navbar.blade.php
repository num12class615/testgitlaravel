<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <h1 class="navbar-text @yield('title-class')"> @yield('title')</h1>
</nav>