<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @section('css')
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <style>
            .btn-delete {
                margin-left: 5px;
            }
        </style>
    @show
    <title>Tung @yield('title')</title>
</head>
<body>
    @include('layout.navbar')
    <br>
    {{-- <h1>Master @yield('title')</h1> --}}
    {{-- @section('sidebar')
        This is the master sidebar.
    @show --}}
    <div class="container">
        {{-- @if (isset($id))
            {{ $id }}
        @else
            <h2>not found</h2>
        @endif

        {{ isset($id) ? $id | 'not found' }}

        {{ $id ?? 'not found' }} --}}
        
        @yield('content')
        {{-- <table class="table table-dark">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>fname</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($people as $p)
                        <tr>
                            <td>{{ $p->id }}</td>
                            <td>{{ $p->fname }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table> --}}
    </div>
    @yield('js')
</body>
</html>