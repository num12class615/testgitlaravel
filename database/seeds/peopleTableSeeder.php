<?php

use Illuminate\Database\Seeder;

class peopleTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 5; $i++) { 
            DB::table('people')->insert([
                'fname'=>$faker->firstName,
                'lname'=>$faker->lastName,
                'age'=>$faker->numberBetween($min = 1, $max = 100),
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
        }
    }
}
