<?php

use Illuminate\Database\Seeder;

class testerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tester')->insert([
            [
                'name'=>'tung',
                'nickname'=>'tung'
            ]
        ]);
    }
}
